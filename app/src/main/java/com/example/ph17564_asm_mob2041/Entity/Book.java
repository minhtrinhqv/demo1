package com.example.ph17564_asm_mob2041.Entity;

public class Book {
    public int bookId;
    public String bookName;
    public int price;
    public int bookTypeId;

    public Book() {
    }

    public Book(int bookId, String bookName, int price, int bookTypeId) {
        this.bookId = bookId;
        this.bookName = bookName;
        this.price = price;
        this.bookTypeId = bookTypeId;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getBookTypeId() {
        return bookTypeId;
    }

    public void setBookTypeId(int bookTypeId) {
        this.bookTypeId = bookTypeId;
    }
}
