package com.example.ph17564_asm_mob2041.Entity;

public class Librarian {
    public String librarianId;
    public String libName;
    public String password;

    public Librarian() {
    }

    public Librarian(String librarianId, String libName, String password) {
        this.librarianId = librarianId;
        this.libName = libName;
        this.password = password;
    }

    public String getLibrarianId() {
        return librarianId;
    }

    public void setLibrarianId(String librarianId) {
        this.librarianId = librarianId;
    }

    public String getLibName() {
        return libName;
    }

    public void setLibName(String libName) {
        this.libName = libName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
