package com.example.ph17564_asm_mob2041.Entity;

public class Member {
    public int memberId;
    public String memberName;
    public String yearOfBirth;

    public Member() {
    }

    public Member(int memberId, String memberName, String yearOfBirth) {
        this.memberId = memberId;
        this.memberName = memberName;
        this.yearOfBirth = yearOfBirth;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getYearOfBirth() {
        return yearOfBirth;
    }

    public void setYearOfBirth(String yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }
}
