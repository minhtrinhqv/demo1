package com.example.ph17564_asm_mob2041.Entity;

public class BookType {
    public int bookTypeId;
    public String bookType;

    public BookType() {
    }

    public BookType(int bookTypeId, String bookType) {
        this.bookTypeId = bookTypeId;
        this.bookType = bookType;
    }

    public int getBookTypeId() {
        return bookTypeId;
    }

    public void setBookTypeId(int bookTypeId) {
        this.bookTypeId = bookTypeId;
    }

    public String getBookType() {
        return bookType;
    }

    public void setBookType(String bookType) {
        this.bookType = bookType;
    }
}
