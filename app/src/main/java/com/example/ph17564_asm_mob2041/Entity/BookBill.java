package com.example.ph17564_asm_mob2041.Entity;

import java.sql.Date;

public class BookBill {
    public int billId;
    public String librarianId;
    public int memberId;
    public int bookId;
    public int price;
    public int returned;
    public Date date;

    public BookBill() {
    }

    public BookBill(int billId, String librarianId, int memberId, int bookId, int price, int returned, Date date) {
        this.billId = billId;
        this.librarianId = librarianId;
        this.memberId = memberId;
        this.bookId = bookId;
        this.price = price;
        this.returned = returned;
        this.date = date;
    }

    public int getBillId() {
        return billId;
    }

    public void setBillId(int billId) {
        this.billId = billId;
    }

    public String getLibrarianId() {
        return librarianId;
    }

    public void setLibrarianId(String librarianId) {
        this.librarianId = librarianId;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getReturned() {
        return returned;
    }

    public void setReturned(int returned) {
        this.returned = returned;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
