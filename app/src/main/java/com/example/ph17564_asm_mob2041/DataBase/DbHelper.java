package com.example.ph17564_asm_mob2041.DataBase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper {
    static final String dbname="PNLIB";
    static final int dvVersion=1;
    public DbHelper(Context context) {
        super(context,dbname,null,dvVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {


        String createTableLibrarian=
                "create table Librarian (" +
                        "librarianId TEXT PRIMARY KEY, " +
                        "libName TEXT NOT NULL, " +
                        "password TEXT NOT NULL )";
        db.execSQL(createTableLibrarian);

        String createTableMember=
                "create table Member (" +
                        "memberId INTEGER PRIMARY KEY , " +
                        "memberName TEXT NOT NULL, " +
                        "yearOfBirth TEXT NOT NULL )";
        db.execSQL(createTableMember);

        String createTableBookType=
                "create table BookType (" +
                        "bookTypeId INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "bookType TEXT NOT NULL )";
        db.execSQL(createTableBookType);

        String createTableBook=
                "create table Book (" +
                        "bookId INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "bookName TEXT NOT NULL, " +
                        "price INTEGER NOT NULL, "+
                        "bookTypeId INTEGER REFERENCES bookType(bookTypeId))";
        db.execSQL(createTableBook);

        String createTableBookBill=
                "create table BookBill (" +
                        "billId INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "price INTEGER NOT NULL, " +
                        "date DATE NOT NULL, " +
                        "returned INTEGER NOT NULL, " +
                        "librarianId TEXT REFERENCES Librarian(librarianId), " +
                        "memberId INTEGER REFERENCES Member(memberId), " +
                        "bookId INTEGER REFERENCES Book(bookId))" ;
        db.execSQL(createTableBookBill);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        String dropTableLibrarian = "drop table if exists Librarian";
        db.execSQL(dropTableLibrarian);
        String dropTableMember = "drop table if exists Member";
        db.execSQL(dropTableMember);
        String dropTableBookType = "drop table if exists BookType";
        db.execSQL(dropTableBookType);
        String dropTableBook = "drop table if exists Book";
        db.execSQL(dropTableBook);
        String dropTableBookBill = "drop table if exists BookBill";
        db.execSQL(dropTableBookBill);
    }
}