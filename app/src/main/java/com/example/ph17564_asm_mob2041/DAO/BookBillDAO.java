package com.example.ph17564_asm_mob2041.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.ph17564_asm_mob2041.DataBase.DbHelper;
import com.example.ph17564_asm_mob2041.Entity.BookBill;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class BookBillDAO {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
    private SQLiteDatabase db;

    public BookBillDAO(Context context) {
        DbHelper dbHelper = new DbHelper(context);
        db = dbHelper.getWritableDatabase();
    }

    //insert
    public long insert(BookBill obj){
        ContentValues values = new ContentValues();
        values.put("librarianId",obj.librarianId);
        values.put("memberId",obj.memberId);
        values.put("bookId",obj.bookId);
        values.put("price",obj.price);
        values.put("returned",obj.returned);
        values.put("date", String.valueOf(obj.date));
        return db.insert("BookBill",null,values);
    }

    //update
    public int update(BookBill obj){
        ContentValues values = new ContentValues();
        values.put("librarianId",obj.librarianId);
        values.put("memberId",obj.memberId);
        values.put("bookId",obj.bookId);
        values.put("price",obj.price);
        values.put("returned",obj.returned);
        values.put("date", String.valueOf(obj.date));
        return db.update("Bookbill",values,"billId=?",new String[]{String.valueOf(obj.billId)});
    }

    //delete
    public int delete(String id){
        return db.delete("BookBill","billId=?",new String[]{id});
    }

    // get tat ca data
    public List<BookBill> getAll(){
        String sql = "SELECT * FROM BookBill";
        return getData(sql);
    }



    //getData theo id
    public BookBill getID(String id){
        String sql = "SELECT * FROM BookBill WHERE billId=?";
        List<BookBill> bookBillList = getData(sql,id);
        return bookBillList.get(0);
    }
    private List<BookBill> getData(String sql, String...selectionArgs) {
        List<BookBill> bookBillList = new ArrayList<>();
        Cursor c = db.rawQuery(sql,selectionArgs);
        while (c.moveToNext()){
            BookBill obj = new BookBill();
            obj.billId = Integer.parseInt(c.getString(c.getColumnIndex("billId")));
            obj.librarianId = c.getString(c.getColumnIndex("librarianId"));
            obj.memberId = Integer.parseInt(c.getString(c.getColumnIndex("memberId")));
            obj.bookId = Integer.parseInt(c.getString(c.getColumnIndex("bookId")));
            obj.price = Integer.parseInt(c.getString(c.getColumnIndex("price")));
            obj.returned = Integer.parseInt(c.getString(c.getColumnIndex("returned")));
            obj.date = java.sql.Date.valueOf(c.getString(c.getColumnIndex("date")));
            bookBillList.add(obj);
        }
        return bookBillList;
    }
    public int checkBook(){
        int check = 1;
        String getBook = "SELECT * FROM Book";
        Cursor cursor = db.rawQuery(getBook,null);
        if (cursor.getCount()!=0){
            check = -1;
        }
        return check;
    }
    public int checkTV(){
        int check = 1;
        String getTV = "SELECT * FROM Member";
        Cursor cursor = db.rawQuery(getTV,null);
        if (cursor.getCount()!=0){
            check =-1;
        }
        return check;
    }


}
