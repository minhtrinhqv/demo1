package com.example.ph17564_asm_mob2041.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.ph17564_asm_mob2041.DataBase.DbHelper;
import com.example.ph17564_asm_mob2041.Entity.Book;

import java.util.ArrayList;
import java.util.List;

public class BookDAO {
    private SQLiteDatabase db;

    public BookDAO(Context context) {
        DbHelper dbHelper = new DbHelper(context);
        db = dbHelper.getWritableDatabase();
    }

    //insert
    public long insert(Book obj) {
        ContentValues values = new ContentValues();
        values.put("bookName", obj.bookName);
        values.put("price", obj.price);
        values.put("bookTypeId", obj.bookTypeId);
        return db.insert("Book", null, values);
    }

    //update
    public int update(Book obj) {
        ContentValues values = new ContentValues();
        values.put("bookName", obj.bookName);
        values.put("price", obj.price);
        values.put("bookTypeId", obj.bookTypeId);
        return db.update("Book", values, "bookId=?", new String[]{String.valueOf(obj.bookId)});
    }

    //delete
    public int delete(String id) {
        return db.delete("Book", "bookId=?", new String[]{id});
    }

    // get tat ca data
    public List<Book> getAll() {
        String sql = "SELECT * FROM Book";
        return getData(sql);
    }


    //getData theo id
    public Book getID(String id) {
        String sql = "SELECT * FROM Book WHERE bookId=?";
        List<Book> list = getData(sql, id);
        return list.get(0);
    }

    private List<Book> getData(String sql, String... selectionArgs) {
        List<Book> list = new ArrayList<>();
        Cursor c = db.rawQuery(sql, selectionArgs);
        while (c.moveToNext()) {
            Book obj = new Book();
            obj.bookId = Integer.parseInt(c.getString(c.getColumnIndex("bookId")));
            obj.bookName = c.getString(c.getColumnIndex("bookName"));
            obj.price = Integer.parseInt(c.getString(c.getColumnIndex("price")));
            obj.bookTypeId = Integer.parseInt(c.getString(c.getColumnIndex("bookTypeId")));
            list.add(obj);
        }
        return list;
    }
    public int checkBookType(){
        int check = 1;
        String getLS = "SELECT * FROM BookType";
        Cursor cursor = db.rawQuery(getLS,null);
        if (cursor.getCount()!=0){
            check = -1;
        }
        return check;
    }
}
