package com.example.ph17564_asm_mob2041.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.ph17564_asm_mob2041.DataBase.DbHelper;
import com.example.ph17564_asm_mob2041.Entity.Librarian;

import java.util.ArrayList;
import java.util.List;

public class LibrarianDAO {
    private SQLiteDatabase db;

    public LibrarianDAO(Context context) {
        DbHelper dbHelper = new DbHelper(context);
        db = dbHelper.getWritableDatabase();
    }

    //insert
    public long insert(Librarian obj) {
        ContentValues values = new ContentValues();
        values.put("librarianId", obj.librarianId);
        values.put("libName", obj.libName);
        values.put("password", obj.password);
        return db.insert("Librarian", null, values);
    }

    //update
    public int update(Librarian obj) {
        ContentValues values = new ContentValues();
        values.put("librarianId", obj.librarianId);
        values.put("libName", obj.libName);
        values.put("password", obj.password);
        return db.update("Librarian", values, "librarianId=?", new String[]{String.valueOf(obj.librarianId)});
    }

    //delete
    public int delete(String id) {
        return db.delete("Librarian", "librarianId=?", new String[]{id});
    }

    // get tat ca data
    public List<Librarian> getAll() {
        String sql = "SELECT * FROM Librarian";
        return getData(sql);
    }


    //getData theo id
    public Librarian getID(String id) {
        String sql = "SELECT * FROM Librarian WHERE librarianId=?";
        List<Librarian> list = getData(sql, id);
        return list.get(0);
    }

    private List<Librarian> getData(String sql, String... selectionArgs) {
        List<Librarian> list = new ArrayList<>();
        Cursor c = db.rawQuery(sql, selectionArgs);
        while (c.moveToNext()) {
            Librarian obj = new Librarian();
            obj.librarianId = c.getString(c.getColumnIndex("librarianId"));
            obj.libName = c.getString(c.getColumnIndex("libName"));
            obj.password = c.getString(c.getColumnIndex("password"));
            list.add(obj);
        }
        return list;
    }
    public boolean checkLibrarian(){
        String getLibrarian = "SELECT * FROM Librarian";
        Cursor cursor = db.rawQuery(getLibrarian,null);
        if (cursor.getCount()==0){
            return true;
        }
        else {
            return false;

        }
    }
    public boolean checkAcc(String strUser, String strPass) {
        String getAccount = "SELECT * FROM Librarian WHERE librarianId = '"+strUser+"' " +
                "AND password = '"+strPass+"'";
        Cursor cursor = db.rawQuery(getAccount,null);
        if (cursor.getCount()!=0){
            return true;
        }
        else {
            return false;

        }
    }
}

