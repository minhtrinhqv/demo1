package com.example.ph17564_asm_mob2041.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.ph17564_asm_mob2041.DataBase.DbHelper;
import com.example.ph17564_asm_mob2041.Entity.BookType;

import java.util.ArrayList;
import java.util.List;

public class BookTypeDAO {
    private SQLiteDatabase db;

    public BookTypeDAO(Context context) {
        DbHelper dbHelper = new DbHelper(context);
        db = dbHelper.getWritableDatabase();
    }

    //insert
    public long insert(BookType obj) {
        ContentValues values = new ContentValues();
        values.put("bookType", obj.bookType);
        return db.insert("BookType", null, values);
    }

    //update
    public int update(BookType obj) {
        ContentValues values = new ContentValues();
        values.put("bookType", obj.bookType);
        return db.update("BookType", values, "bookTypeId=?", new String[]{String.valueOf(obj.bookTypeId)});
    }

    //delete
    public int delete(String id) {
        return db.delete("BookType", "bookTypeId=?", new String[]{id});
    }

    // get tat ca data
    public List<BookType> getAll() {
        String sql = "SELECT * FROM BookType";
        return getData(sql);
    }

    //getData theo id
    public BookType getID(String id) {
        String sql = "SELECT * FROM BookType WHERE bookTypeId=?";
        List<BookType> list = getData(sql, id);
        return list.get(0);
    }

    private List<BookType> getData(String sql, String... selectionArgs) {
        List<BookType> list = new ArrayList<>();
        Cursor c = db.rawQuery(sql, selectionArgs);
        while (c.moveToNext()) {
            BookType obj = new BookType();
            obj.bookTypeId = Integer.parseInt(c.getString(c.getColumnIndex("bookTypeId")));
            obj.bookType = c.getString(c.getColumnIndex("bookType"));
            list.add(obj);
        }
        return list;
    }
}
