package com.example.ph17564_asm_mob2041.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.ph17564_asm_mob2041.DataBase.DbHelper;
import com.example.ph17564_asm_mob2041.Entity.Member;

import java.util.ArrayList;
import java.util.List;

public class MemberDAO {
    private SQLiteDatabase db;

    public MemberDAO(Context context) {
        DbHelper dbHelper = new DbHelper(context);
        db = dbHelper.getWritableDatabase();
    }

    //insert
    public long insert(Member obj){
        ContentValues values = new ContentValues();
        values.put("memberName",obj.memberName);
        values.put("yearOfBirth",obj.yearOfBirth);

        return db.insert("Member",null,values);
    }

    //update
    public int update(Member obj){
        ContentValues values = new ContentValues();
        values.put("memberName",obj.memberName);
        values.put("yearOfBirth",obj.yearOfBirth);

        return db.update("Member",values,"memberId=?",new String[]{String.valueOf(obj.memberId)});
    }

    //delete
    public int delete(String id){
        return db.delete("Member","memberId=?",new String[]{id});
    }

    // get tat ca data
    public List<Member> getAll(){
        String sql = "SELECT * FROM Member";
        return getData(sql);
    }



    //getData theo id
    public Member getID(String id){
        String sql = "SELECT * FROM Member WHERE memberId=?";
        List<Member> memberList = getData(sql,id);
        return memberList.get(0);
    }
    private List<Member> getData(String sql, String...selectionArgs) {
        List<Member> list = new ArrayList<>();
        Cursor c = db.rawQuery(sql,selectionArgs);
        while (c.moveToNext()){
            Member obj = new Member();
            obj.memberId = Integer.parseInt(c.getString(c.getColumnIndex("memberId")));
            obj.memberName = c.getString(c.getColumnIndex("memberName"));
            obj.yearOfBirth = c.getString(c.getColumnIndex("yearOfBirth"));
            list.add(obj);
        }
        return list;
    }


}

