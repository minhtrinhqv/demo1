package com.example.ph17564_asm_mob2041.DAO;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.ph17564_asm_mob2041.DataBase.DbHelper;
import com.example.ph17564_asm_mob2041.Entity.Book;
import com.example.ph17564_asm_mob2041.Entity.Top;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class StatisticsDAO {
    private SQLiteDatabase db;
    private Context context;
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

    public StatisticsDAO(Context context) {
        this.context = context;
        DbHelper dbHelper = new DbHelper(context);
        db = dbHelper.getWritableDatabase();
    }
    //top10
    public List<Top> getTop() {
        String sqlTop = "SELECT bookId, count(bookId) as count FROM BookBill " +
                "GROUP BY bookId ORDER BY count DESC LIMIT 10";
        List<Top> topList = new ArrayList<>();
        BookDAO bookDAO = new BookDAO(context);
        Cursor c = db.rawQuery(sqlTop,null);
        while (c.moveToNext()) {
            Top top = new Top();
            Book book = bookDAO.getID(c.getString(c.getColumnIndex("bookId")));
            top.bookName = book.bookName;
            top.count = Integer.parseInt(c.getString(c.getColumnIndex("count")));
            topList.add(top);

        }
        return topList;


    }
    //doanh thu
    public int getStatistics(String fromDay,String toDay){
        String sqlStatistics = "SELECT SUM(price) as statistics FROM BookBill " +
                "WHERE date BETWEEN ? AND ?";
        List<Integer> list = new ArrayList<>();
        Cursor c = db.rawQuery(sqlStatistics,new String[]{fromDay,toDay});
        while (c.moveToNext()){
            try {
                list.add(Integer.parseInt(c.getString(c.getColumnIndex("statistics"))));
            }
            catch (Exception e){
                list.add(0);
            }
        }
        return list.get(0);
    }
}
