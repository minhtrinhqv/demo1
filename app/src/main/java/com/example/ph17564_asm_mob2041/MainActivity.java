package com.example.ph17564_asm_mob2041;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.ph17564_asm_mob2041.DAO.LibrarianDAO;
import com.example.ph17564_asm_mob2041.Entity.Librarian;
import com.example.ph17564_asm_mob2041.ui.RevenueFragment;
import com.example.ph17564_asm_mob2041.ui.ChangePassFragment;
import com.example.ph17564_asm_mob2041.ui.BookTypeFragment;
import com.example.ph17564_asm_mob2041.ui.BookBillFragment;
import com.example.ph17564_asm_mob2041.ui.BookFragment;
import com.example.ph17564_asm_mob2041.ui.MemberFragment;
import com.example.ph17564_asm_mob2041.ui.AddUserFragment;
import com.example.ph17564_asm_mob2041.ui.TopFragment;
import com.google.android.material.navigation.NavigationView;

public class MainActivity extends AppCompatActivity {
    DrawerLayout drawerLayout;
    Toolbar toolbar;
    View mheaderView;
    TextView eduser;
    LibrarianDAO librarianDAO;
    FragmentManager manager;
    Librarian librarian;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        drawerLayout = findViewById(R.id.drawer_layout);
        toolbar = findViewById(R.id.toolbar);

        //set toolbar
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,
                R.string.navigation_drawer_open,R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        //PhieuMuonFragment lam home
        setTitle("Phiếu mượn ");
        manager = getSupportFragmentManager();
        BookBillFragment bookBillFragment = new BookBillFragment();
        manager.beginTransaction()
                .replace(R.id.flContent,bookBillFragment)
                .commit();
        NavigationView nv = findViewById(R.id.nav_view);

        //lay user
        mheaderView = nv.getHeaderView(0);
        eduser = mheaderView.findViewById(R.id.tvUser);
        Intent i = getIntent();
        String user = i.getStringExtra("user");
        librarianDAO = new LibrarianDAO(this);
        librarian = librarianDAO.getID(user);
        String userName = librarian.libName;
        eduser.setText("Welcome "+userName+"!");
        //admin co quyen truy cap

          if(user.equalsIgnoreCase("admin")){
            nv.getMenu().findItem(R.id.sub_addUser).setVisible(true);
          }

        nv.setNavigationItemSelectedListener(item -> {
             manager = getSupportFragmentManager();
            switch (item.getItemId()){
                case R.id.nav_PhieuMuon:
                    setTitle("Phiếu mượn ");
                    BookBillFragment phieuMuonFragment1 = new BookBillFragment();
                    manager.beginTransaction()
                            .replace(R.id.flContent,phieuMuonFragment1)
                            .commit();
                    break;
                case R.id.nav_LoaiSach:
                    setTitle("Loại sách ");
                    BookTypeFragment loaiSachFragment = new BookTypeFragment();
                    manager.beginTransaction()
                            .replace(R.id.flContent,loaiSachFragment)
                            .commit();
                    break;
                case R.id.nav_Sach:
                    setTitle("Sách ");
                    BookFragment sachFragment = new BookFragment();
                    manager.beginTransaction()
                            .replace(R.id.flContent,sachFragment)
                            .commit();
                    break;
                case R.id.nav_ThanhVien:
                    setTitle("Thành viên ");
                    MemberFragment thanhVienFragment = new MemberFragment();
                    manager.beginTransaction()
                            .replace(R.id.flContent,thanhVienFragment)
                            .commit();
                    break;
                case R.id.sub_Top:
                    setTitle("Top 10 đầu sách");
                    TopFragment topFragment = new TopFragment();
                    manager.beginTransaction()
                            .replace(R.id.flContent,topFragment)
                            .commit();
                    break;
                case R.id.sub_DoanhThu:
                    setTitle("Thống kê doanh thu");
                    RevenueFragment doanhThuFragment = new RevenueFragment();
                    manager.beginTransaction()
                            .replace(R.id.flContent,doanhThuFragment)
                            .commit();
                    break;
                case R.id.sub_addUser:
                    setTitle("Tạo tài khoản ");
                    AddUserFragment themMoiFragment = new AddUserFragment();
                    manager.beginTransaction()
                            .replace(R.id.flContent,themMoiFragment)
                            .commit();
                    break;
                case R.id.sub_Pass:
                    setTitle("Đổi mật khẩu ");
                    ChangePassFragment doiMatKhauFragment = new ChangePassFragment();
                    manager.beginTransaction()
                            .replace(R.id.flContent,doiMatKhauFragment)
                            .commit();
                    break;
                case R.id.sub_LogOut:
                    startActivity(new Intent(getApplicationContext(),LoginActivity.class));
                    finish();
                    break;
            }
            drawerLayout.closeDrawers();
            return false;
        });



    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if(id==android.R.id.home){
            drawerLayout.openDrawer(GravityCompat.START);
        }
        return super.onOptionsItemSelected(item);
    }
}