package com.example.ph17564_asm_mob2041.ui;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.ph17564_asm_mob2041.DAO.StatisticsDAO;
import com.example.ph17564_asm_mob2041.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class RevenueFragment extends Fragment {
    Button btnFromDay,btnToDay,btnRevenue;
    EditText edFromDay,edToDay;
    TextView tvRevenue;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    int mYear,mMonth,mDay;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_revenue, container, false);
        edFromDay = v.findViewById(R.id.edFromDay);
        edToDay = v.findViewById(R.id.edToDay);
        tvRevenue = v.findViewById(R.id.tvRevenue);
        btnFromDay = v.findViewById(R.id.btnFromDay);
        btnToDay = v.findViewById(R.id.btnToDay);
        btnRevenue = v.findViewById(R.id.btnRevenue);

        DatePickerDialog.OnDateSetListener mDateFrom = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mYear = year;
                mMonth = month;
                mDay = dayOfMonth;
                GregorianCalendar c= new GregorianCalendar(mYear,mMonth,mDay);
                edFromDay.setText(sdf.format(c.getTime()));
            }
        };
        DatePickerDialog.OnDateSetListener mDateDenNgay = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mYear = year;
                mMonth = month;
                mDay = dayOfMonth;
                GregorianCalendar c= new GregorianCalendar(mYear,mMonth,mDay);
                edToDay.setText(sdf.format(c.getTime()));
            }
        };
        btnFromDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog d = new DatePickerDialog(getActivity(),
                        0,mDateFrom,mYear,mMonth,mDay);
                d.show();
            }
        });
        btnToDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog d = new DatePickerDialog(getActivity(),
                        0,mDateDenNgay,mYear,mMonth,mDay);
                d.show();
            }
        });
        btnRevenue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String fromDay = edFromDay.getText().toString();
                String toDay = edToDay.getText().toString();
                StatisticsDAO statisticsDAO = new StatisticsDAO(getActivity());
                tvRevenue.setText("Doanh thu: "+statisticsDAO.getStatistics(fromDay,toDay)+"VNĐ");
            }
        });
        return v;
    }

}