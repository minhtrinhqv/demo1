package com.example.ph17564_asm_mob2041.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.example.ph17564_asm_mob2041.Adapter.BookTypeSpinnerAdapter;
import com.example.ph17564_asm_mob2041.Adapter.BookAdapter;
import com.example.ph17564_asm_mob2041.DAO.BookTypeDAO;
import com.example.ph17564_asm_mob2041.DAO.BookDAO;
import com.example.ph17564_asm_mob2041.Entity.BookType;
import com.example.ph17564_asm_mob2041.Entity.Book;
import com.example.ph17564_asm_mob2041.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

public class BookFragment extends Fragment {
    ListView lv;
    ArrayList<Book> list;
    FloatingActionButton fab;
    Dialog dialog;
    EditText edBookId,edBookName,edPrice;
    Spinner spBookType;
    Button btnSave,btnCancel;
    static BookDAO dao;
    BookAdapter adapter;
    Book item;
    BookTypeSpinnerAdapter spinnerAdapter;
    ArrayList<BookType> listBookTypes;
    BookTypeDAO bookTypeDAO;
    BookType bookType;
    int bookTypeId,position;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_book,container,false);
        lv = v.findViewById(R.id.lvBook);
        fab = v.findViewById(R.id.fab);
        dao = new BookDAO(getActivity());
        capNhatLv();
        fab.setOnClickListener(v1 -> {
            if (dao.checkBookType()>0){
                Toast.makeText(getContext(),"Không tìm thấy loại sách",Toast.LENGTH_SHORT).show();
            }
            else {
                openDialog(getActivity(), 0);
            }
            });
        lv.setOnItemLongClickListener((parent, view, position, id) -> {
            item = list.get(position);
            openDialog(getActivity(),1);
            return false;
        });
        return v;
    }
    protected void openDialog(final Context context, final int type){
        dialog = new Dialog(context);
        dialog.setContentView(R.layout.book_dialog);
        edBookId = dialog.findViewById(R.id.edBookId);
        edBookName = dialog.findViewById(R.id.edBookName);
        edPrice = dialog.findViewById(R.id.edPrice);
        edPrice.setInputType(InputType.TYPE_CLASS_NUMBER);
        spBookType = dialog.findViewById(R.id.spBookType);
        btnSave = dialog.findViewById(R.id.btnSave);
        btnCancel = dialog.findViewById(R.id.btnCancel);

        listBookTypes = new ArrayList<>();
        bookTypeDAO = new BookTypeDAO(context);
        listBookTypes = (ArrayList<BookType>)bookTypeDAO.getAll();
        spinnerAdapter = new BookTypeSpinnerAdapter(context, listBookTypes);
        spBookType.setAdapter(spinnerAdapter);
        //lay ma loai
        spBookType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                bookTypeId = listBookTypes.get(position).bookTypeId;
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        edBookId.setEnabled(false);
        if (type!=0){
            edBookId.setText(String.valueOf(item.bookId));
            edBookName.setText(item.bookName);
            edPrice.setText(String.valueOf(item.price));
            for (int i = 0; i< listBookTypes.size(); i++)
                if (item.bookTypeId==(listBookTypes.get(i).bookTypeId)){
                    position = i;
                }
            Log.i("demo","posSach"+position);
                spBookType.setSelection(position);
        }
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate()>0){
                item = new Book();
                item.bookName = edBookName.getText().toString();
                item.price = Integer.parseInt(edPrice.getText().toString());
                item.bookTypeId = bookTypeId;

                    if (type==0){
                        if (dao.insert(item)>0){
                            Toast.makeText(context,"Thêm thành công",Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(context,"Thêm thất bại",Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        item.bookId = Integer.parseInt(edBookId.getText().toString());
                        if (dao.update(item)>0){
                            Toast.makeText(context,"Sửa thành công",Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(context,"Sửa thất bại",Toast.LENGTH_SHORT).show();
                        }
                    }
                    capNhatLv();
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }
    public void xoa(final String id){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Delete");
        builder.setMessage("Bạn có muốn xóa không ?");
        builder.setCancelable(true);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dao.delete(id);
                capNhatLv();
                dialog.cancel();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert  = builder.create();
        builder.show();
    }
    void capNhatLv(){
        list = (ArrayList<Book>)dao.getAll();
        adapter = new BookAdapter(getActivity(),this,list);
        lv.setAdapter(adapter);
    }
    private int validate(){
        int check = 1;
        if(edBookName.getText().length()==0||edPrice.getText().length()==0){
            Toast.makeText(getContext(),"Bạn phải nhập đủ thông tin",Toast.LENGTH_SHORT).show();
            check=-1;
        }
        else if (edPrice.getText().length()!=0){
            int gia = Integer.parseInt(edPrice.getText().toString());
            if (gia<0||gia>500000){
                Toast.makeText(getContext(),"Bạn phải nhập đúng giá thuê",Toast.LENGTH_SHORT).show();
                check=-1;
            }
        }
        return check;
    }
}