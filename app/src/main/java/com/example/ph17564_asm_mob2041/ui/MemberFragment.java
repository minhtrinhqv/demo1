package com.example.ph17564_asm_mob2041.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.example.ph17564_asm_mob2041.Adapter.MemberAdapter;
import com.example.ph17564_asm_mob2041.DAO.MemberDAO;
import com.example.ph17564_asm_mob2041.Entity.Member;
import com.example.ph17564_asm_mob2041.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;


public class MemberFragment extends Fragment {
    ListView lv;
    ArrayList<Member> list;
    FloatingActionButton fab;
    Dialog dialog;
    EditText edMemberId,edMemberName,edYearOfBirth;
    Button btnSave,btnCancel;
    MemberDAO dao;
    MemberAdapter adapter;
    Member item;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_member,container,false);
        lv = v.findViewById(R.id.lvMember);
        fab = v.findViewById(R.id.fab);
        dao = new MemberDAO(getActivity());
        capNhatLv();
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog(getActivity(),0);
            }
        });
        lv.setOnItemLongClickListener((parent, view, position, id) -> {
            item = list.get(position);
            openDialog(getActivity(),1);
            return false;
        });
        return v;
    }
    protected void openDialog(final Context context,final int type){
        dialog = new Dialog(context);
        dialog.setContentView(R.layout.member_dialog);
        edMemberId = dialog.findViewById(R.id.edMemberId);
        edMemberName = dialog.findViewById(R.id.edMemberName);
        edYearOfBirth = dialog.findViewById(R.id.edYearOfBirth);
        edYearOfBirth.setInputType(InputType.TYPE_CLASS_NUMBER);
        btnSave = dialog.findViewById(R.id.btnSave);
        btnCancel = dialog.findViewById(R.id.btnCancel);
        edMemberId.setEnabled(false);
        if (type!=0){
            edMemberId.setText(String.valueOf(item.memberId));
            edMemberName.setText(item.memberName);
            edYearOfBirth.setText(item.yearOfBirth);
        }
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item = new Member();
                item.memberName = edMemberName.getText().toString();
                item.yearOfBirth = edYearOfBirth.getText().toString();
                if (validate()>0){
                    if (type==0){
                        if (dao.insert(item)>0){
                            Toast.makeText(context,"Thêm thành công",Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(context,"Thêm thất bại",Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        item.memberId = Integer.parseInt(edMemberId.getText().toString());
                        if (dao.update(item)>0){
                            Toast.makeText(context,"Sửa thành công",Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(context,"Sửa thất bại",Toast.LENGTH_SHORT).show();
                        }
                    }
                    capNhatLv();
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }
    public void xoa(final String id){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Delete");
        builder.setMessage("Bạn có muốn xóa không ?");
        builder.setCancelable(true);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dao.delete(id);
                capNhatLv();
                dialog.cancel();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert  = builder.create();
        builder.show();
    }
    void capNhatLv(){
        list = (ArrayList<Member>) dao.getAll();
        adapter = new MemberAdapter(getActivity(),this,list);
        lv.setAdapter(adapter);
    }
    private int validate(){
        int check = 1;
        if(edMemberName.getText().length()==0||edYearOfBirth.getText().length()==0){
            Toast.makeText(getContext(),"Bạn phải nhập đủ thông tin",Toast.LENGTH_SHORT).show();
            check=-1;
        }
        else if (edYearOfBirth.getText().length()!=0){
            int namSinh = Integer.parseInt(edYearOfBirth.getText().toString());
            if (namSinh<1900||namSinh>2021){
                Toast.makeText(getContext(),"Bạn phải nhập đúng năm sinh",Toast.LENGTH_SHORT).show();
                check=-1;
            }
        }
        return check;
    }
}