package com.example.ph17564_asm_mob2041.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.example.ph17564_asm_mob2041.DAO.LibrarianDAO;
import com.example.ph17564_asm_mob2041.Entity.Librarian;
import com.example.ph17564_asm_mob2041.R;
import com.google.android.material.textfield.TextInputEditText;


public class AddUserFragment extends Fragment {
    LibrarianDAO dao;
    TextInputEditText edUserNew,edNameNew,edUPassNew,edReUPassNew;
    Button btnSaveUN,btnCancelUN;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_add_user, container, false);
        edUserNew = v.findViewById(R.id.edUserNew);
        edNameNew = v.findViewById(R.id.edNameNew);
        edUPassNew = v.findViewById(R.id.edUPassNew);
        edReUPassNew = v.findViewById(R.id.edReUPassNew);
        btnSaveUN = v.findViewById(R.id.btnSaveUN);
        btnCancelUN = v.findViewById(R.id.btnCancelUN);
        dao = new LibrarianDAO(getActivity());

        btnCancelUN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edUserNew.setText("");
                edNameNew.setText("");
                edUPassNew.setText("");
                edReUPassNew.setText("");
            }
        });
        btnSaveUN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Librarian librarian = new Librarian();
                librarian.librarianId =edUserNew.getText().toString();
                librarian.libName = edNameNew.getText().toString();
                librarian.password = edUPassNew.getText().toString();
                if (validate()>0){
                    if (dao.insert(librarian)>0){
                        Toast.makeText(getContext(),"Lưu thành công",Toast.LENGTH_SHORT).show();
                        edUserNew.setText("");
                        edNameNew.setText("");
                        edUPassNew.setText("");
                        edReUPassNew.setText("");
                    }
                    else{
                        Toast.makeText(getContext(),"Lưu thất bại",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
        return v;
    }
    private int validate(){
        int check = 1;
        if (edUserNew.getText().length()==0||edNameNew.getText().length()==0||edUPassNew.getText().length()==0||edReUPassNew.getText().length()==0)
        {
            Toast.makeText(getContext(),"Bạn phải nhập đầy đủ thông tin",Toast.LENGTH_SHORT).show();
            check = -1;
        }
        else
        {
            String pass= edUPassNew.getText().toString();
            String rePass = edReUPassNew.getText().toString();

            if (!pass.equals(rePass)){
                Toast.makeText(getContext(),"Mật khẩu không trùng khớp",Toast.LENGTH_SHORT).show();
            }
        }
        return check;
    }
}