package com.example.ph17564_asm_mob2041.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.fragment.app.Fragment;

import com.example.ph17564_asm_mob2041.Adapter.TopAdapter;
import com.example.ph17564_asm_mob2041.DAO.StatisticsDAO;
import com.example.ph17564_asm_mob2041.Entity.Top;
import com.example.ph17564_asm_mob2041.R;

import java.util.ArrayList;

public class TopFragment extends Fragment {
    ListView listView;
    ArrayList<Top> list;
    TopAdapter adapter;
    StatisticsDAO statisticsDAO;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_top, container, false);
        listView = v.findViewById(R.id.lvTop);
        statisticsDAO = new StatisticsDAO(getActivity());
        capNhatLv();
        return v;
    }
    void capNhatLv(){
        list = (ArrayList)statisticsDAO.getTop();
        adapter = new TopAdapter(getActivity(),this,list);
        listView.setAdapter(adapter);
    }
}