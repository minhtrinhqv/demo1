package com.example.ph17564_asm_mob2041.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.example.ph17564_asm_mob2041.Adapter.BookBillAdapter;
import com.example.ph17564_asm_mob2041.Adapter.BookSpinnerAdapter;
import com.example.ph17564_asm_mob2041.Adapter.MemberSpinnerAdapter;
import com.example.ph17564_asm_mob2041.DAO.BookBillDAO;
import com.example.ph17564_asm_mob2041.DAO.BookDAO;
import com.example.ph17564_asm_mob2041.DAO.MemberDAO;
import com.example.ph17564_asm_mob2041.Entity.BookBill;
import com.example.ph17564_asm_mob2041.Entity.Book;
import com.example.ph17564_asm_mob2041.Entity.Member;
import com.example.ph17564_asm_mob2041.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class BookBillFragment extends Fragment {

    ListView lv;
    ArrayList<BookBill> list;
    FloatingActionButton fab;
    Dialog dialog;
    EditText edBillId;
    Spinner spMemberName,spBookName;
    TextView tvPrice,tvDate;
    CheckBox chkReturned;
    Button btnSave,btnCancel;
    static BookBillDAO dao;
    BookBillAdapter adapter;
    BookBill item;
    MemberSpinnerAdapter memberSpinnerAdapter;
    ArrayList<Member> listMember;
    MemberDAO memberDAO;
    Member member;
    int memberId;
    BookSpinnerAdapter bookSpinnerAdapter;
    ArrayList<Book> listBook;
    BookDAO bookDAO;
    Book book;
    int bookId,price;
    int positionTV,positionBook;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_book_bill,container,false);
        lv = v.findViewById(R.id.lvBookBill);
        fab = v.findViewById(R.id.fab);
        dao = new BookBillDAO(getActivity());
        capNhatLv();
        fab.setOnClickListener(v1 -> {
            if (dao.checkTV()>0){
                Toast.makeText(getContext(),"Không tìm thấy thành viên",Toast.LENGTH_SHORT).show();
            }
            else if (dao.checkBook()>0){
                Toast.makeText(getContext(),"Không tìm thấy sách",Toast.LENGTH_SHORT).show();
            }
            else {
                openDialog(getActivity(),0);
            }
        });
        lv.setOnItemLongClickListener((parent, view, position, id) -> {
            item = list.get(position);
            openDialog(getActivity(),1);//update
            return false;
        });
        return v;
    }
    protected void openDialog(final Context context, final int type){
        dialog = new Dialog(context);
        dialog.setContentView(R.layout.bookbill_dialog);
        edBillId = dialog.findViewById(R.id.edBillId);
        spMemberName = dialog.findViewById(R.id.spMemberName);
        spBookName = dialog.findViewById(R.id.spBookName);
        tvDate = dialog.findViewById(R.id.tvDate);
        tvPrice = dialog.findViewById(R.id.tvPrice);
        chkReturned = dialog.findViewById(R.id.chkReturned);
        btnSave = dialog.findViewById(R.id.btnSave);
        btnCancel = dialog.findViewById(R.id.btnCancel);

        //lay ma thanh vien
        memberDAO = new MemberDAO(context);
        listMember = new ArrayList<Member>();
        listMember = (ArrayList<Member>)memberDAO.getAll();
        memberSpinnerAdapter = new MemberSpinnerAdapter(context,listMember);
        spMemberName.setAdapter(memberSpinnerAdapter);
        spMemberName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                memberId = listMember.get(position).memberId;
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        //lay ma sach
        bookDAO = new BookDAO(context);
        listBook = new ArrayList<Book>();
        listBook = (ArrayList<Book>)bookDAO.getAll();
        bookSpinnerAdapter = new BookSpinnerAdapter(context,listBook);
        spBookName.setAdapter(bookSpinnerAdapter);
        spBookName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                bookId = listBook.get(position).bookId;
                price = listBook.get(position).price;
                tvPrice.setText("Tiền thuê: "+price);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        tvDate.setText("Ngày thuê: "+now());

        //ktra type 0 or 1
        edBillId.setEnabled(false);
        if (type!=0){
            edBillId.setText(String.valueOf(item.billId));
            for (int i =0;i<listMember.size();i++)
                if (item.memberId==(listMember.get(i).memberId)){
                    positionTV = i;
                }
            spMemberName.setSelection(positionTV);
            for (int i =0;i<listBook.size();i++)
                if (item.bookId==(listBook.get(i).bookId)){
                    positionBook = i;
                }
            spBookName.setSelection(positionBook);
            tvDate.setText("Ngày thuê: "+item.date);
            tvPrice.setText("Tiền thuê:" +item.price);
            if (item.returned==1){
                chkReturned.setChecked(true);
            }
            else {
                chkReturned.setChecked(false);
            }
        }
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                item = new BookBill();
                item.bookId = bookId;
                item.memberId = memberId;
                item.date = java.sql.Date.valueOf(now());
                item.price = price;
                if (chkReturned.isChecked()){
                    item.returned = 1;
                }
                else {
                    item.returned =0;
                }
                if (validate()>0){
                    if (type==0){
                        if (dao.insert(item)>0){
                            Toast.makeText(context,"Thêm thành công",Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(context,"Thêm thất bại",Toast.LENGTH_SHORT).show();
                        }
                    }
                    else {
                        item.billId = Integer.parseInt(edBillId.getText().toString());
                        if (dao.update(item)>0){
                            Toast.makeText(context,"Sửa thành công",Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(context,"Sửa thất bại",Toast.LENGTH_SHORT).show();
                        }
                    }
                    capNhatLv();
                    dialog.dismiss();
                }
            }
        });
        dialog.show();
    }

    private String now(){
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String date = sdf.format(Calendar.getInstance().getTime());
        return date;
    }

    public void xoa(final String id){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Delete");
        builder.setMessage("Bạn có muốn xóa không ?");
        builder.setCancelable(true);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dao.delete(id);
                capNhatLv();
                dialog.cancel();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        AlertDialog alert  = builder.create();
        builder.show();
    }
    java.util.Date date;
    void capNhatLv(){
        list = (ArrayList<BookBill>)dao.getAll();
        adapter = new BookBillAdapter(getActivity(),this,list);
        lv.setAdapter(adapter);
    }
    private int validate(){
        int check = 1;
        return check;
    }
}