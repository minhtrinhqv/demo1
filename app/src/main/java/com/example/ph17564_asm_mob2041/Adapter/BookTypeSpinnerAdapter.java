package com.example.ph17564_asm_mob2041.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.ph17564_asm_mob2041.Entity.BookType;
import com.example.ph17564_asm_mob2041.R;

import java.util.ArrayList;

public class BookTypeSpinnerAdapter extends ArrayAdapter<BookType> {
    private Context context;
    private ArrayList<BookType> lists;
    TextView tvBookTypeIdSp,tvBookTypeSp;

    public BookTypeSpinnerAdapter(@NonNull Context context, ArrayList<BookType> lists) {
        super(context, 0,lists);
        this.context = context;
        this.lists = lists;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;
        if (v==null){
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.booktype_item_spinner,null);
        }
        final BookType item = lists.get(position);
        if (item!=null){
            tvBookTypeIdSp = v.findViewById(R.id.tvBookTypeIdSp);
            tvBookTypeIdSp.setText(item.bookTypeId+".");
            tvBookTypeSp = v.findViewById(R.id.tvBookTypeSp);
            tvBookTypeSp.setText(item.bookType);
        }
        return v;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;
        if (v==null){
            LayoutInflater inflater=(LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.booktype_item_spinner,null);
        }
        final BookType item = lists.get(position);
        if (item!=null){
            tvBookTypeIdSp = v.findViewById(R.id.tvBookTypeIdSp);
            tvBookTypeIdSp.setText(item.bookTypeId+".");
            tvBookTypeSp = v.findViewById(R.id.tvBookTypeSp);
            tvBookTypeSp.setText(item.bookType);
        }
        return v;
    }
}
