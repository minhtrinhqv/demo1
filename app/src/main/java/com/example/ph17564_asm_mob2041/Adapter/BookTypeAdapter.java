package com.example.ph17564_asm_mob2041.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.ph17564_asm_mob2041.Entity.BookType;
import com.example.ph17564_asm_mob2041.R;
import com.example.ph17564_asm_mob2041.ui.BookTypeFragment;


import java.util.ArrayList;

public class BookTypeAdapter extends ArrayAdapter<BookType> {
    private Context context;
    BookTypeFragment fragment;
    private ArrayList<BookType> lists;
    TextView tvBookTypeId,tvBookType;
    ImageView imgDelBT;

    public BookTypeAdapter(@NonNull Context context, BookTypeFragment fragment, ArrayList<BookType> lists) {
        super(context, 0,lists);
        this.context = context;
        this.lists = lists;
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;
        if (v == null){
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v=inflater.inflate(R.layout.booktype_item,null);
        }
        final BookType item = lists.get(position);
        if (item != null){

            tvBookTypeId = v.findViewById(R.id.tvBookTypeId);
            tvBookTypeId.setText("Mã loại sách: "+item.bookTypeId);
            tvBookType = v.findViewById(R.id.tvBookType);
            tvBookType.setText("Tên loại sách: "+item.bookType);

            imgDelBT = v.findViewById(R.id.imgDelBT);
        }
        imgDelBT.setOnClickListener(v1 -> {
            fragment.xoa(String.valueOf(item.bookTypeId));
        });
        return v;
    }
}
