package com.example.ph17564_asm_mob2041.Adapter;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.ph17564_asm_mob2041.Entity.Member;
import com.example.ph17564_asm_mob2041.R;

import java.util.ArrayList;
public class MemberSpinnerAdapter extends ArrayAdapter<Member> {
    private Context context;
    private ArrayList<Member> lists;
    TextView tvMemberIdSp,tvMemberNameSp;

    public MemberSpinnerAdapter(@NonNull Context context, ArrayList<Member> lists) {
        super(context, 0,lists);
        this.context = context;
        this.lists = lists;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;
        if (v==null){
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.member_item_spinner,null);
        }
        final Member item = lists.get(position);
        if (item!=null){
            tvMemberIdSp = v.findViewById(R.id.tvMemberIdSp);
            tvMemberIdSp.setText(item.memberId+".");
            tvMemberNameSp = v.findViewById(R.id.tvMemberNameSp);
            tvMemberNameSp.setText(item.memberName);
        }
        return v;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;
        if (v==null){
            LayoutInflater inflater=(LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.member_item_spinner,null);
        }
        final Member item = lists.get(position);
        if (item!=null){
            tvMemberIdSp = v.findViewById(R.id.tvMemberIdSp);
            tvMemberIdSp.setText(item.memberId+".");
            tvMemberNameSp = v.findViewById(R.id.tvMemberNameSp);
            tvMemberNameSp.setText(item.memberName);
        }
        return v;
    }
}
