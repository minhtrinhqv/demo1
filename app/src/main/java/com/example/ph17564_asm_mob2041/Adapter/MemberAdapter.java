package com.example.ph17564_asm_mob2041.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.ph17564_asm_mob2041.Entity.Member;
import com.example.ph17564_asm_mob2041.R;
import com.example.ph17564_asm_mob2041.ui.MemberFragment;

import java.util.ArrayList;

public class MemberAdapter extends ArrayAdapter<Member> {
    private Context context;
    MemberFragment fragment;
    private ArrayList<Member> lists;
    TextView tvMemberId,tvMemberName,tvYearOfBirth;
    ImageView imgDelM;

    public MemberAdapter(@NonNull Context context, MemberFragment fragment, ArrayList<Member> lists) {
        super(context, 0,lists);
        this.context = context;
        this.lists = lists;
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;
        if (v == null){
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v=inflater.inflate(R.layout.member_item,null);
        }
        final Member item = lists.get(position);
        if (item != null){

            tvMemberId = v.findViewById(R.id.tvMemberId);
            tvMemberId.setText("Mã thành viên: "+item.memberId);
            tvMemberName = v.findViewById(R.id.tvMemberName);
            tvMemberName.setText("Tên thành viên: "+item.memberName);
            tvYearOfBirth = v.findViewById(R.id.tvYearOfBirth);
            tvYearOfBirth.setText("Năm sinh: "+item.yearOfBirth);

            imgDelM = v.findViewById(R.id.imgDelM);
        }
        imgDelM.setOnClickListener(v1 -> {
            fragment.xoa(String.valueOf(item.memberId));
        });
        return v;
    }
}
