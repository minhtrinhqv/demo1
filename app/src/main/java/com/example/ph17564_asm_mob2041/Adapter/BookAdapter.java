package com.example.ph17564_asm_mob2041.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


import com.example.ph17564_asm_mob2041.DAO.BookTypeDAO;
import com.example.ph17564_asm_mob2041.Entity.BookType;
import com.example.ph17564_asm_mob2041.Entity.Book;
import com.example.ph17564_asm_mob2041.R;
import com.example.ph17564_asm_mob2041.ui.BookFragment;

import java.util.ArrayList;

public class BookAdapter extends ArrayAdapter<Book> {
    private Context context;
    BookFragment fragment;
    private ArrayList<Book> lists;
    TextView tvBookId,tvBookName,tvPrice,tvBookTypeId;
    ImageView imgDel;

    public BookAdapter(@NonNull Context context, BookFragment fragment, ArrayList<Book> lists) {
        super(context, 0,lists);
        this.context = context;
        this.lists = lists;
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;
        if (v == null){
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v=inflater.inflate(R.layout.book_item,null);
        }
        final Book item = lists.get(position);
        if (item != null){
            BookTypeDAO loaiSachDAO =  new BookTypeDAO(context);
            BookType bookType = loaiSachDAO.getID(String.valueOf(item.bookTypeId));
            String bookTypeNAme = bookType.bookType;
            tvBookId = v.findViewById(R.id.tvBookId);
            tvBookId.setText("Mã sách: "+item.bookId);
            tvBookName = v.findViewById(R.id.tvBookName);
            tvBookName.setText("Tên sách: "+item.bookName);
            tvPrice = v.findViewById(R.id.tvPrice);
            tvPrice.setText("Giá thuê: "+item.price);
            tvBookTypeId = v.findViewById(R.id.tvBookTypeId);
            tvBookTypeId.setText("Loại sách: "+bookTypeNAme);

            imgDel = v.findViewById(R.id.imgDelB);
        }
        imgDel.setOnClickListener(v1 -> {
            fragment.xoa(String.valueOf(item.bookId));
        });
        return v;
    }
}

