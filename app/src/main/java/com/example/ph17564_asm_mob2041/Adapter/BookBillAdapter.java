package com.example.ph17564_asm_mob2041.Adapter;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.ph17564_asm_mob2041.DAO.BookDAO;
import com.example.ph17564_asm_mob2041.DAO.MemberDAO;
import com.example.ph17564_asm_mob2041.Entity.BookBill;
import com.example.ph17564_asm_mob2041.Entity.Book;
import com.example.ph17564_asm_mob2041.Entity.Member;
import com.example.ph17564_asm_mob2041.R;
import com.example.ph17564_asm_mob2041.ui.BookBillFragment;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
public class BookBillAdapter extends ArrayAdapter<BookBill> {
    private Context context;
    BookBillFragment fragment;
    private ArrayList<BookBill> lists;
    TextView tvBillId,tvMemberId,tvBookId,tvPrice,tvReturned,tvDate;
    ImageView imgDelBB;
    BookDAO bookDAO;
    MemberDAO memberDAO;
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    public BookBillAdapter(@NonNull Context context, BookBillFragment fragment, ArrayList<BookBill> lists) {
        super(context, 0,lists);
        this.context = context;
        this.lists = lists;
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;
        if (v == null){
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v=inflater.inflate(R.layout.bookbill_item,null);
        }
        final BookBill item = lists.get(position);
        if (item != null){

            tvBillId = v.findViewById(R.id.tvBillId);
            tvBillId.setText("Mã phiếu: "+item.bookId);
            bookDAO  = new BookDAO(context);
            Book book = bookDAO.getID(String.valueOf(item.bookId));
            tvBookId = v.findViewById(R.id.tvBookId);
            tvBookId.setText("Tên sách: "+book.bookName);
            memberDAO  = new MemberDAO(context);
            Member member = memberDAO.getID(String.valueOf(item.memberId));
            tvMemberId = v.findViewById(R.id.tvMemberId);
            tvMemberId.setText("Thành viên: "+member.memberName);
            tvPrice = v.findViewById(R.id.tvPrice);
            tvPrice.setText("Tiền thuê: "+item.price);
            tvDate = v.findViewById(R.id.tvDate);
            tvDate.setText("Ngày thuê: "+item.date);
            //+sdf.format(item.ngay)
            tvReturned = v.findViewById(R.id.tvReturned);
            if (item.returned==1){
                tvReturned.setText("Đã trả sách");
                tvReturned.setTextColor(Color.BLUE);
            }else {
                tvReturned.setTextColor(Color.RED);
                tvReturned.setText("Chưa trả sách");
            }
            imgDelBB = v.findViewById(R.id.imgDelBB);
        }
        imgDelBB.setOnClickListener(v1 -> {
            fragment.xoa(String.valueOf(item.billId));
        });
        return v;
    }
}

